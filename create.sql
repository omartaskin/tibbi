create database cengiz;

use cengiz;

CREATE TABLE `data` (
  `DATA_ID` int(11) NOT NULL AUTO_INCREMENT,
  `TYPE` varchar(20) NOT NULL,
  `VALUE` smallint(6) NOT NULL,
  `IDATE` date NOT NULL,
  PRIMARY KEY (`DATA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
