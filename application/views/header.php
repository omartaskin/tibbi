<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, shrink-to-fit=no, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Simple Sidebar - Start Bootstrap Template</title>
    <link href="<?php echo base_url(); ?>static/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="<?php echo base_url(); ?>static/css/simple-sidebar.css" rel="stylesheet"/>
    <script src="<?php echo base_url(); ?>static/jquery/dist/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>static/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<div id="wrapper">

    <!-- Sidebar -->
    <div id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <li class="sidebar-brand">
                <a href="<?php echo base_url(); ?>">
                    Şeker Takip Sistemi
                </a>
            </li>
            <li>
                <a href="<?php echo base_url() ?>index.php/save">Yeni Ölçüm Verisi Ekle</a>
            </li>
            <li>
                <a href="<?php echo base_url() ?>index.php/history">Geçmiş Veriler</a>
            </li>
            <li>
                <a href="<?php echo base_url() ?>index.php/stats">İstatistik</a>
            </li>
        </ul>
    </div>
    <!-- /#sidebar-wrapper -->
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">