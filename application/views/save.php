<?php $this->load->view('header'); ?>
    <h3>Yeni Veri Girişi</h3>
    <p style="margin-top:30px;">
    <form class="form-inline" method="POST">
        <div class="form-group">
            <input type="text" name="value" class="form-control" id="exampleInputEmail3" placeholder="Ölçüm verisi">
        </div>
        <div class="radio">
            <label>
                <input type="radio" name="type" id="dateMorning" value="Sabah" aria-label="Sabah"
                       checked="checked"> Sabah
            </label>
            <label>
                <input type="radio" name="type" id="dateNoon" value="Öğle" aria-label="Öğle"> Öğle
            </label>
            <label>
                <input type="radio" name="type" id="dateEvening" value="Akşam" aria-label="Akşam">
                Akşam
            </label>
        </div>
        <button type="submit" class="btn btn-default">Kaydet</button>
    </form>
    </p>

    <p class="bg-warning">
        Sağlıklı bir bireyin açlık şekeri oranı 70-100 aralığında seyretmektedir!
    </p>
<?php $this->load->view('footer'); ?>