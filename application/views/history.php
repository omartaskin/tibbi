<?php $this->load->view('header'); ?>
    <h3>Geçmiş Veriler ve Risk Durumları</h3>

    <table class="table table-striped">
        <tr>
            <th>Tarih</th>
            <th>Ölçüm Verisi</th>
            <th>Zaman Dilimi</th>
        </tr>
        <?php foreach ($history as $h): ?>
            <tr <?php if ($h->VALUE > 100 && ($h->VALUE) < 130): ?> class="warning" <?php elseif ($h->VALUE > 130 || $h->VALUE < 70): ?>class="danger"  <?php endif; ?>>
                <td><?php echo $h->IDATE ?></td>
                <td><?php echo $h->VALUE ?></td>
                <td><?php echo $h->TYPE ?></td>
            </tr>
        <?php endforeach; ?>
    </table>

    <script type="text/javascript" src="<?php echo base_url() ?>static/js/canvasjs.min.js"></script>
    <script type="text/javascript">
        window.onload = function () {
            var chart = new CanvasJS.Chart("chartContainer",
                {
                    animationEnabled: true,
                    theme: "theme4",
                    title: {
                        text: "Değişim Grafiği"
                    },
                    axisY: [{
                        lineColor: "#4F81BC",
                        tickColor: "#4F81BC",
                        labelFontColor: "#4F81BC",
                        titleFontColor: "#4F81BC",
                        lineThickness: 2,
                    },
                        {
                            lineColor: "#C0504E",
                            tickColor: "#C0504E",
                            labelFontColor: "#C0504E",
                            titleFontColor: "#C0504E",
                            lineThickness: 2,

                        }],
                    data: [
                        {
                            type: "spline", //change type to bar, line, area, pie, etc
                            showInLegend: true,
                            dataPoints: [
                                <?php $data = array();
                                foreach ($history as $i => $h):
                                    $data[] = " {x: " . ($i + 1) . ", y: " . $h->VALUE . "}";
                                endforeach;
                                echo implode(',', $data);?>
                            ]
                        }
                    ],
                    legend: {
                        cursor: "pointer",
                        itemclick: function (e) {
                            if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                                e.dataSeries.visible = false;
                            } else {
                                e.dataSeries.visible = true;
                            }
                            chart.render();
                        }
                    }
                });

            chart.render();
        }
    </script>
    <div id="chartContainer" style="height: 300px; width: 100%;"></div>
<?php $this->load->view('footer'); ?>