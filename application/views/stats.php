<?php $this->load->view('header'); ?>
    <h3>İstatistikler</h3>

    <h5>Genel Ortalama</h5>
    <table class="table table-striped">
        <tr>
            <th>Ortalama</th>
        </tr>
        <tr <?php if ($avg > 100 && ($avg) < 130): ?> class="warning" <?php elseif ($avg > 130 || $avg < 70): ?>class="danger"  <?php endif; ?>>
            <td><?php echo $avg ?></td>
        </tr>
    </table>

    <h5>Gün Bazlı</h5>
    <table class="table table-striped">
        <tr>
            <th>Tarih</th>
            <th>Ortalama Değer</th>
        </tr>
        <?php foreach ($dailyAvg as $avg): ?>
            <tr <?php if ($avg['AVG'] > 100 && ($avg['AVG']) < 130): ?> class="warning" <?php elseif ($avg['AVG'] > 130 || $avg['AVG'] < 70): ?>class="danger"  <?php endif; ?>>
                <td><?php echo $avg['IDATE'] ?></td>
                <td><?php echo $avg['AVG'] ?></td>
            </tr>
        <?php endforeach; ?>
    </table>


    <h5>Zaman Dilimi Bazlı</h5>
    <table class="table table-striped">
        <tr>
            <th>Zaman Dilimi</th>
            <th>Ortalama Değer</th>
        </tr>
        <?php foreach ($typelyAvg as $h): ?>
            <tr <?php if ($h['AVG'] > 100 && ($h['AVG']) < 130): ?> class="warning" <?php elseif ($h['AVG'] > 130 || $h['AVG'] < 70): ?>class="danger"  <?php endif; ?>>
                <td><?php echo $h['TYPE'] ?></td>
                <td><?php echo $h['AVG'] ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
    <script type="text/javascript">
        $(function () {
            Highcharts.chart('container', {
                title: {
                    text: 'Öğün / Şeker Değişim Grafiği',
                    x: -20 //center
                },
                xAxis: {
                    categories: [<?php echo implode(',', $categories); ?>]
                },
                yAxis: {
                    title: {
                        text: 'Şeker Değeri'
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle',
                    borderWidth: 0
                },
                series: [
                    <?php $texts = array();
                    foreach ($categorisedResults as $type => $results) {
                        $texts[] = " {
                        name: '$type',
                        data: [" . implode(',', $results) . "]
                        }";

                    } echo implode(',', $texts);?>
                ]
            });
        });
    </script>

    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>

    <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
<?php $this->load->view('footer'); ?>