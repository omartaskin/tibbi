<?php

/**
 * Created by IntelliJ IDEA.
 * User: otaskin
 * Date: 08/12/2016
 * Time: 23:26
 */
class Stats extends CI_Controller
{
    public function index()
    {
        $avg = $this->data_model->getAvg();
        $dailyAvg = $this->data_model->getDailyAvg();
        $typelyAvg = $this->data_model->getTypelyAvg();
        $avgByType = $this->data_model->getAvgByType();

        $categories = array();
        $categorisedResults = array();
        foreach ($avgByType as $a) {
            $categories[$a['IDATE']] = '"' . $a['IDATE'] . '"';
            $categorisedResults[$a['TYPE']][$a['IDATE']] = $a['AVG'];
        }

        foreach ($categorisedResults as $name => $dates) {
            $diffs = array_diff(array_keys($categories), array_keys($dates));
            if (count($diffs) > 0) {
                foreach ($diffs as $diff) {
                    $categorisedResults[$name][$diff] = 0;
                }
            }
        }

        $this->load->view('stats', array(
            'avg' => $avg['AVG'],
            'dailyAvg' => $dailyAvg,
            'typelyAvg' => $typelyAvg,
            'categories' => $categories,
            'categorisedResults' => $categorisedResults,
        ));

    }
}