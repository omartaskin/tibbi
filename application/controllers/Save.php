<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Save extends CI_Controller
{
    public function index()
    {
        if (!empty($this->input->post())) {

            $this->data_model->save($this->input->post('value'), $this->input->post('type'), date('Y-m-d'));
            redirect('/history', 'refresh');
            return;
        }

        $this->load->view('save');
    }
}
