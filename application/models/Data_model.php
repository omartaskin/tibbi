<?php

class Data_model extends CI_Model
{
    private $table = 'data';

    public function __construct()
    {
        parent::__construct();
    }

    public function get()
    {
        $results = array();
        $query = $this->db->get($this->table);

        foreach ($query->result() as $row) {
            $results[] = $row;
        }

        return $results;
    }

    public function save($value, $type, $idate)
    {
        $this->db->insert($this->table, array('VALUE' => $value, 'TYPE' => $type, 'IDATE' => $idate));
    }

    public function getAvg()
    {
        $query = $this->db->query("SELECT AVG(VALUE) AS AVG FROM data");
        foreach ($query->result_array() as $row) {
            return $row;
        }
    }

    public function getTypelyAvg()
    {
        $result = array();

        $query = $this->db->query("SELECT AVG(VALUE) AS AVG,TYPE FROM data GROUP BY(TYPE)");
        foreach ($query->result_array() as $row) {
            $result[] = $row;
        }

        return $result;
    }

    public function getDailyAvg()
    {
        $result = array();

        $query = $this->db->query("SELECT AVG(VALUE) AS AVG, IDATE FROM data GROUP BY(IDATE)  ORDER BY IDATE, TYPE");
        foreach ($query->result_array() as $row) {
            $result[] = $row;
        }

        return $result;
    }

    public function getAvgByType()
    {
        $result = array();

        $query = $this->db->query("SELECT AVG(VALUE) AS AVG, TYPE, IDATE FROM data GROUP BY TYPE, IDATE ORDER BY IDATE, TYPE DESC");
        foreach ($query->result_array() as $row) {
            $result[] = $row;
        }

        return $result;
    }
}